This is an example module implementing a request subscriber for Amazon
Echo Alexa Skill. 
This module is converted from node.js program
( https://github.com/acalpixca/BarcelonaQuiz )to Drupal module.

For more detail, please refer to the following original page.
http://lavigilanta.info/voice/en/barcelona-quiz-a-fun-way-to-prepare-your-holidays/

Note:
- Alexa module( https://www.drupal.org/project/alexa ) is required.
- You must disable "Alexa Demo" module.

Setup:
- Enable "Alexa Quiz" module.
- Setup Amazon Alexa Skills Kit according to
https://www.drupal.org/node/2701403.

- Edit Quiz name ( admin/config/services/alexa_quiz ).
- Edit Quiz data file (QuizData.php).
